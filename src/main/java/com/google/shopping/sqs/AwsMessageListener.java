package com.google.shopping.sqs;

import com.amazon.sqs.javamessaging.message.SQSTextMessage;
import com.google.gson.Gson;
import com.google.shopping.domain.GMCCommon;
import com.google.shopping.dto.products.AuthData;
import com.google.shopping.dto.products.ProductsDto;
import com.google.shopping.exception.GoogleProductException;
import com.google.shopping.service.products.UploadService;
import com.google.shopping.utils.ReportUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by user on 2017-12-12.
 */

@RequiredArgsConstructor
@Component
public class AwsMessageListener implements MessageListener{
    private final UploadService uploadService;
    private final ReportUtils reportUtils;
    private final AuthData authData;

    private final Gson gson = new Gson();
    private ThreadPoolExecutor executor;

    @PostConstruct
    private void init(){
        int threadCount = 4;
        executor = new ThreadPoolExecutor(threadCount, threadCount, 0L,
                TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>());
    }

    @PreDestroy
    private void destroy(){
        try {
            executor.shutdown();
            if(!executor.awaitTermination(5, TimeUnit.SECONDS)){
                executor.shutdown();
            }
        }catch (InterruptedException e){
            throw new GoogleProductException(e.getMessage(), e);
        }
    }

    @Override
    public void onMessage(Message message) {
        SQSTextMessage sqsMessage = (SQSTextMessage) message;
        try {
             String idValue = sqsMessage.getJMSMessagePropertyValue("id").getStringMessageAttributeValue();
             int id = Integer.parseInt(idValue);
             String product = sqsMessage.getText();
             if(id == 1){
                reportUtils.startProcess(GMCCommon.GOOGLE_PRODUCT_UPLOAD, String.valueOf(authData.getMerchantId()));
             }

            executor.execute(() -> {
                ProductsDto[] productsDtoList = gson.fromJson(product, ProductsDto[].class);
                uploadService.uploadProducts(new ArrayList<>(Arrays.asList(productsDtoList)));
            });
        }catch (JMSException e){
            throw new GoogleProductException(e.getMessage(), e);
        }
    }
}
