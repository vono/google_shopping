package com.google.shopping.sqs;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.model.MessageAttributeValue;
import com.amazonaws.services.sqs.model.SendMessageBatchRequest;
import com.amazonaws.services.sqs.model.SendMessageBatchRequestEntry;
import com.amazonaws.services.sqs.model.SendMessageBatchResult;
import com.google.gson.Gson;
import com.google.shopping.dto.products.ProductsDto;
import com.google.shopping.exception.GoogleProductException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by user on 2017-12-11.
 */
@RequiredArgsConstructor
@Component
public class SqsProducer {
    private  final AmazonSQS amazonSQS;

    @Value("${aws.sqs.url}")
    private  String requestQueueUrl;

    public  void pushToSqs(List<ProductsDto> productsList, int id){
        try {
            Map<String, MessageAttributeValue> stringMessageAttributeValueMap = new HashMap<>();
            MessageAttributeValue messageAttributeValue = new MessageAttributeValue().withDataType("String").withStringValue(String.valueOf(id));
            stringMessageAttributeValueMap.put("id", messageAttributeValue);

            String listString = new Gson().toJson(productsList);
            List<SendMessageBatchRequestEntry> entries = new ArrayList<>();
            entries.add(new SendMessageBatchRequestEntry()
                    .withId(Integer.toString(id))
                    .withMessageAttributes(stringMessageAttributeValueMap)
                    .withMessageBody(listString));

            SendMessageBatchRequest sqsRequest  = new SendMessageBatchRequest()
                    .withQueueUrl(requestQueueUrl)
                    .withEntries(entries);

            SendMessageBatchResult sendMessageBatchResult = amazonSQS.sendMessageBatch(sqsRequest);
            int failedSize = sendMessageBatchResult.getFailed().size();
        }catch (Exception e){
            throw new GoogleProductException(e.getMessage(), e);
        }
    }
}
