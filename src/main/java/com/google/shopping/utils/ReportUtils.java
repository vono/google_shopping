package com.google.shopping.utils;

import com.google.api.services.content.model.Errors;
import com.google.gson.Gson;
import com.google.shopping.domain.*;
import com.google.shopping.dto.products.ProductsDto;
import io.evanwong.oss.hipchat.v2.HipChatClient;
import io.evanwong.oss.hipchat.v2.rooms.MessageColor;
import io.evanwong.oss.hipchat.v2.rooms.SendRoomNotificationRequestBuilder;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by user on 2017-12-13.
 */
@RequiredArgsConstructor
@Component
@Slf4j
public class ReportUtils {
    private final HistoryRepository historyRepository;
    private final CommonErrorRepository commonErrorRepository;
    private final HipChatClient hipChatClient;

    private List<Map<String, String>> missingList = new ArrayList<>();
    private List<Map<String, String>> uploadFailList = new ArrayList<>();
    private CommonError commonError;
    private String historyId;
    private String productUploadId;

    @Value("${hipchat.room-id}")
    private String hipchatRoomId;

    public void startProcess(String processType, String merchantId){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String currentTime = dateFormat.format(new Date(System.currentTimeMillis()));

        History history = new History();
        history.setMerchantId(merchantId);
        history.setTime(currentTime);
        history.setType(processType);
        History response = historyRepository.save(history);

        if(processType.equals(GMCCommon.GOOGLE_PRODUCT_UPLOAD)){
            productUploadId = response.getId();
        }else{
            historyId = response.getId();
        }

        sendNotification(processType + " task has been started - " + response.getId(), MessageColor.GREEN);
    }


    public void messageWithFail(String type, String errorMessage){
        commonError = new CommonError();
        if(type.equals(GMCCommon.GOOGLE_PRODUCT_UPLOAD)) {
            commonError.setHistoryId(productUploadId);
        }else {
            commonError.setHistoryId(historyId);
        }
        commonError.setErrorType(type);

        List<Map<String, String>> errorList = new ArrayList<>();
        Map<String, String> errorMsg = new HashMap<>();
        errorMsg.put("message", errorMessage);
        errorList.add(errorMsg);
        commonError.setMessage(errorList);
        commonErrorRepository.save(commonError);

        sendNotification(type + " Error : " + errorMessage + " - " + commonError.getHistoryId(), MessageColor.RED);
    }


    void requiredMissing(String missingLine, String missingColumn){
        Map<String, String> missingData = new HashMap<>();
        missingData.put("line", missingLine);
        missingData.put("error_column", missingColumn);
        missingList.add(missingData);
    }

    void saveRequiredError(){
        if(missingList != null && missingList.size() > 0){
            commonError = new CommonError();
            commonError.setHistoryId(historyId);
            commonError.setErrorType(GMCCommon.EP_TSV_PARSING);
            commonError.setMessage(missingList);
            commonErrorRepository.save(commonError);
            missingList = new ArrayList<>();
        }
    }


    public void uploadFail(Errors errors, ProductsDto productData, boolean isLast){
        if(errors != null && errors.size() > 0){
            commonError = new CommonError();
            commonError.setHistoryId(productUploadId);
            commonError.setErrorType(GMCCommon.GOOGLE_PRODUCT_UPLOAD);

            Map<String, String> errorMsg = new HashMap<>();
            errorMsg.put("id", String.valueOf(productData.getBatchId()));
            errorMsg.put("error_code", String.valueOf(errors.getCode()));
            errorMsg.put("error_message", errors.getMessage());
            errorMsg.put("errors", new Gson().toJson(errors));
            errorMsg.put("product_data", new Gson().toJson(productData));
            uploadFailList.add(errorMsg);
        }

        if(isLast){
            commonError.setMessage(uploadFailList);
            commonErrorRepository.save(commonError);
            uploadFailList = new ArrayList<>();
            sendNotification(GMCCommon.GOOGLE_PRODUCT_UPLOAD + " task has been failed - " + productUploadId, MessageColor.RED);
        }

        assert errors != null;
        System.out.println("batchId : " + productData.getBatchId() + " ,  error Msg : " + errors.getMessage() + " , error code : " + errors.getCode()
                + " , error : " + errors.getErrors() + " , productData title : " + productData.getTitle());
    }

    private void sendNotification(String message, MessageColor color){
        SendRoomNotificationRequestBuilder notiBuilder =
                hipChatClient.prepareSendRoomNotificationRequestBuilder(hipchatRoomId, message);
        notiBuilder.setNotify(true).setColor(color).build().execute();
    }

}
