package com.google.shopping.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;

import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Component;

import com.google.shopping.domain.GMCCommon;
import com.google.shopping.dto.products.ProductsDto;
import com.google.shopping.exception.GoogleProductException;
import com.google.shopping.sqs.SqsProducer;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;


/**
 * Created by user on 2017-11-29.
 */
@RequiredArgsConstructor
@Component
@Slf4j
public class FileUtils {
    private final SqsProducer sqsProducer;
    private final ReportUtils reportUtils;
    private final S3Utils s3Utils;

    private static final String CONDITION_NEW = "신상품";
    private static final String CONDITION_REFURBISHED = "리퍼";
    private static final String IN_STOCK = "in stock";
    private static final String OUT_OF_STOCK = "out of stock";
    private static final int MAX_NUM = 100;

    public String downloadFiles(String merchantId, String fileUrl){
        reportUtils.startProcess(GMCCommon.EP_DOWNLOAD, merchantId);
        String result;
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmmss");
            String currentTime = dateFormat.format(new Date(System.currentTimeMillis()));

            result = IOUtils.toString(new URL(fileUrl), "UTF-8");
            URL url = new URL((fileUrl));

            InputStream inputStream = url.openStream();
            byte[] bytes = IOUtils.toByteArray(inputStream);
            String folderName = merchantId + "/";
            String fileName = folderName + "gmc_backup_" + merchantId + "_" + currentTime;

            s3Utils.upload(bytes, fileName);

        }catch (Exception e){
            reportUtils.messageWithFail(GMCCommon.EP_DOWNLOAD, e.getMessage());
            throw new GoogleProductException(e.getMessage(), e);
        }
        return result;
    }

    public List<ProductsDto> parseTSV(String result, String merchantId){
        reportUtils.startProcess(GMCCommon.EP_TSV_PARSING, merchantId);
        CSVReader reader = new CSVReaderBuilder(new StringReader(result))
                .withCSVParser(new CSVParserBuilder().withSeparator('\t').build()).build();

        String[] nextLine;
        List<String[]> lineList = new ArrayList<>();

        try {
            while ((nextLine = reader.readNext()) != null){
                lineList.add(nextLine);
            }
        }catch (IOException e){
            throw new GoogleProductException(e.getMessage());
        }


        Map<String, Integer> columnMap = new HashMap<>();
        List<ProductsDto> productsList = new ArrayList<>();

        if(lineList.size() > 0 ){
            for(int i =0; i<lineList.get(0).length; i++){
                String[] strTitle = lineList.get(0);
                columnMap.put(strTitle[i], i);
            }
        }

        int callCnt = 0;
        if(columnMap.size() > 0) {
            for (int i = 1; i < lineList.size(); i++) {
                String[] columnData = lineList.get(i);
                ProductsDto productsDto = new ProductsDto();
                String productType = "";
                boolean isSuccess = true;
                String missingColumn = "";

                if(columnData[columnMap.get("category_name1")].length() > 0){
                    productType = columnData[columnMap.get("category_name1")];

                    if(columnData[columnMap.get("category_name2")].length() > 0){
                        productType += ">" + columnData[columnMap.get("category_name2")];
                    }

                    if(columnData[columnMap.get("category_name3")].length() > 0){
                        productType += ">" + columnData[columnMap.get("category_name3")];
                    }

                    if(columnData[columnMap.get("category_name4")].length() > 0){
                        productType += ">" + columnData[columnMap.get("category_name4")];
                    }
                }else{
                    missingColumn = "category_name1";
                    isSuccess= false;
                }



                String condition = columnData[columnMap.get("condition")];
                if(condition.length() > 0){
                    if(condition.equals(CONDITION_NEW)){
                        condition = "new";
                    }else if(condition.equals(CONDITION_REFURBISHED)){
                        condition = "refurbished";
                    }else{
                        condition = "used";
                    }
                }else{
                    condition = "new";
                }

                String brand = columnData[columnMap.get("brand")];
                if(brand.length() == 0){
                    if(columnData[columnMap.get("maker")].length() > 0){
                        brand = columnData[columnMap.get("maker")];
                    }else{
                        missingColumn = "brand";
                        isSuccess= false;
                    }

                }

                String adult = columnData[columnMap.get("adult")];
                Boolean adultFlag;
                if(adult.length() > 0){
                    if(adult.equals("Y")){
                        adultFlag = true;
                    }else{
                        adultFlag = false;
                    }
                }else{
                    adultFlag = false;
                }

                String availability;
                if(columnMap.get("class") != null){
                    availability = columnData[columnMap.get("class")];
                    if(availability.equals("D")){
                        availability = OUT_OF_STOCK;
                    }else{
                        availability = IN_STOCK;
                    }
                }else{
                    availability = IN_STOCK;
                }

                if(columnData[columnMap.get("id")].length() > 0){
                    productsDto.setId(columnData[columnMap.get("id")]);
                }else{
                    missingColumn = "id";
                    isSuccess= false;
                }

                if(columnData[columnMap.get("title")].length() > 0){
                    productsDto.setTitle(columnData[columnMap.get("title")]);
                    productsDto.setDescription(columnData[columnMap.get("title")]);
                }else{
                    missingColumn = "title";
                    isSuccess= false;
                }

                if(columnData[columnMap.get("link")].length() > 0){
                    productsDto.setLink(columnData[columnMap.get("link")]);
                }else{
                    missingColumn = "link";
                    isSuccess= false;
                }

                if(columnData[columnMap.get("image_link")].length() > 0){
                    productsDto.setImageLink(columnData[columnMap.get("image_link")]);
                }else{
                    missingColumn = "image_link";
                    isSuccess= false;
                }

                if(columnData[columnMap.get("price_pc")].length() > 0){
                    productsDto.setPrice(columnData[columnMap.get("price_pc")]);
                }else{
                    missingColumn = "price_pc";
                    isSuccess= false;
                }

                productsDto.setBrand(brand);
                productsDto.setProductType(productType);
                productsDto.setAvailability(availability);
                productsDto.setCondition(condition);
                productsDto.setAdult(adultFlag);
                productsDto.setBatchId((long) i);

                if(isSuccess){
                    productsList.add(productsDto);
                }else{
                    reportUtils.requiredMissing(String.valueOf(i), missingColumn);
                }

                if(i == lineList.size() - 1){
                    reportUtils.saveRequiredError();
                }

                if(i % MAX_NUM == 0){
                    callCnt++;
                    sqsProducer.pushToSqs(productsList, callCnt);
                    productsList = new ArrayList<>();
                }else{
                    if(i == lineList.size() - 1 && productsList.size() > 0){
                        callCnt++;
                        sqsProducer.pushToSqs(productsList, callCnt);
                    }
                }
            }
        }
        return productsList;
    }

}
