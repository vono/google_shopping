package com.google.shopping.utils;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.*;
import com.google.shopping.exception.GoogleProductException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 2017-12-18.
 */
@RequiredArgsConstructor
@Component
public class S3Utils {
    private final AmazonS3 amazonS3;

    @Value("${aws.s3.bucket-name}")
    private String bucketName;
    @Value("${aws.s3.url}")
    private String url;

    public CompleteMultipartUploadRequest upload(byte[] bytes, String fileName) {
        List<PartETag> partETags = new ArrayList<>();
        InitiateMultipartUploadRequest initRequest = new InitiateMultipartUploadRequest(bucketName, fileName);
        InitiateMultipartUploadResult initResponse = amazonS3.initiateMultipartUpload(initRequest);

        long contentLength = bytes.length;
        long partSize = 5 * 1024 * 1024;
        try {
            long filePosition = 0;
            for (int i = 1; filePosition < contentLength; i++) {
                partSize = Math.min(partSize, (contentLength - filePosition));
                UploadPartRequest uploadRequest = new UploadPartRequest()
                        .withBucketName(bucketName).withKey(fileName)
                        .withUploadId(initResponse.getUploadId()).withPartNumber(i)
                        .withFileOffset(filePosition)
                        .withInputStream(new ByteArrayInputStream(bytes))
                        .withPartSize(partSize);
                partETags.add(amazonS3.uploadPart(uploadRequest).getPartETag());
                filePosition += partSize;
            }

            CompleteMultipartUploadRequest compRequest = new CompleteMultipartUploadRequest(bucketName, fileName,
                    initResponse.getUploadId(), partETags);

            amazonS3.completeMultipartUpload(compRequest);
            return compRequest;
        } catch (Exception e) {
            amazonS3.abortMultipartUpload(new AbortMultipartUploadRequest(
                    bucketName, fileName, initResponse.getUploadId()));
            throw new GoogleProductException(e.getMessage(), e);
        }
    }
}
