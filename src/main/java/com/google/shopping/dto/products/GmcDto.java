package com.google.shopping.dto.products;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by user on 2017-12-28.
 */
public class GmcDto {

    @Getter
    @Setter
    public static class ProductRequest {
        String merchantId;
        String fileUrl;
    }
}
