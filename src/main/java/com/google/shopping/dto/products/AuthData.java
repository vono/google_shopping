package com.google.shopping.dto.products;

import com.google.api.services.content.ShoppingContent;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

/**
 * Created by user on 2017-12-22.
 */

@Getter
@Setter
@Component
public class AuthData {
    private ShoppingContent shoppingContent;
    private long merchantId;
}
