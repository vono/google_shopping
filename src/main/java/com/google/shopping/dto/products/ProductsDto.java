package com.google.shopping.dto.products;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by user on 2017-11-27.
 */

@Getter
@Setter
public class ProductsDto {
    private String id;
    private String title;
    private String description;
    private String link;
    private String imageLink;
    private String availability;
    private String price;
    private String productType;
    private String brand;
    private String condition;
    private Boolean adult;
    private String productClass;
    private long batchId;
}
