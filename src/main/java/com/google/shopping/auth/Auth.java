package com.google.shopping.auth;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.content.ShoppingContent;
import com.google.api.services.content.ShoppingContentScopes;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.shopping.domain.GMCCommon;
import com.google.shopping.domain.GmcMember;
import com.google.shopping.exception.GoogleAuthException;
import com.google.shopping.service.member.MemberService;
import com.google.shopping.utils.ReportUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;

/**
 * Created by user on 2017-11-29.
 */
@Component
@RequiredArgsConstructor
public class Auth {
    private final ReportUtils reportUtils;
    private final MemberService memberService;

    public ShoppingContent getCredential(String merchantId){
        reportUtils.startProcess(GMCCommon.GOOGLE_AUTH, merchantId);
        JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
        Credential credential;
        HttpTransport httpTransport;

        GmcMember keyData = memberService.getMemberData(merchantId);
        String applicationName = keyData.getApplicationName();
        try {
            JsonObject jsonObject = (JsonObject) new JsonParser().parse(keyData.getKeyfile());
            InputStream inputStream = new ByteArrayInputStream(jsonObject.toString().getBytes());

            credential = GoogleCredential.fromStream(inputStream).createScoped(ShoppingContentScopes.all());
            httpTransport = GoogleNetHttpTransport.newTrustedTransport();
        }catch (GeneralSecurityException | IOException e){
            reportUtils.messageWithFail(GMCCommon.GOOGLE_AUTH, e.getMessage());
            throw new GoogleAuthException(e.getMessage(), e);
        }

        ShoppingContent.Builder builder = new ShoppingContent.Builder(
                httpTransport, jsonFactory, credential)
                .setApplicationName(applicationName);
        return builder.build();
    }

}
