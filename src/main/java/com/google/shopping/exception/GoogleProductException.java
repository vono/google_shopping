package com.google.shopping.exception;

/**
 * Created by user on 2017-12-12.
 */
public class GoogleProductException extends RuntimeException{

    public GoogleProductException(String message){
        super(message);
    }

    public GoogleProductException(String message, Throwable throwable){
        super(message, throwable);
    }

}
