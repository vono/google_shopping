package com.google.shopping.exception;

/**
 * Created by user on 2017-12-13.
 */
public class GoogleAuthException extends RuntimeException{

    public GoogleAuthException(String message, Throwable throwable){
        super(message, throwable);
    }
}
