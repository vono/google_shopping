package com.google.shopping.config;

import com.amazon.sqs.javamessaging.ProviderConfiguration;
import com.amazon.sqs.javamessaging.SQSConnectionFactory;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.google.shopping.sqs.AwsMessageListener;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.listener.DefaultMessageListenerContainer;

import javax.jms.ConnectionFactory;

/**
 * Created by user on 2017-12-11.
 */

@Configuration
public class AwsConfig {
    @Value("${aws.access-key}")
    private String awsAccessKey;
    @Value("${aws.secret-key}")
    private String awsSecretKey;
    @Value("${aws.regions}")
    private String regions;

    @Bean
    public AWSStaticCredentialsProvider awsStaticCredentialsProvider(){
        return new AWSStaticCredentialsProvider(new BasicAWSCredentials(awsAccessKey, awsSecretKey));
    }

    @Bean
    public AmazonSQS amazonSQS(){
        return AmazonSQSClientBuilder.standard().withRegion(regions)
                .withCredentials(awsStaticCredentialsProvider()).build();
    }

    @Bean
    public ConnectionFactory connectionFactory(AWSStaticCredentialsProvider awsStaticCredentialsProvider){
        AmazonSQSClientBuilder amazonSQSClientBuilder = AmazonSQSClientBuilder.standard().withRegion(regions);
        amazonSQSClientBuilder.setCredentials(awsStaticCredentialsProvider);
        return new SQSConnectionFactory(new ProviderConfiguration(), amazonSQSClientBuilder);
    }

    @Bean
    public AmazonS3 amazonS3Client(){
        return AmazonS3ClientBuilder.standard().withCredentials(awsStaticCredentialsProvider())
                .withRegion(regions).build();
    }

    @Bean
    DefaultMessageListenerContainer defaultMessageListenerContainer (ConnectionFactory connectionFactory,
                                                                     AwsMessageListener awsMessageListener,
                                                                     @Value("${aws.sqs.name}") String qname) {
        DefaultMessageListenerContainer defaultMessageListenerContainer = new DefaultMessageListenerContainer();
        defaultMessageListenerContainer.setDestinationName(qname);
        defaultMessageListenerContainer.setConcurrency("3-10");
        defaultMessageListenerContainer.setConnectionFactory(connectionFactory);
        defaultMessageListenerContainer.setMessageListener(awsMessageListener);
        return defaultMessageListenerContainer;
    }

}
