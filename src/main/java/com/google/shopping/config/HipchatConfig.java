package com.google.shopping.config;

import io.evanwong.oss.hipchat.v2.HipChatClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by user on 2017-12-26.
 */

@Configuration
public class HipchatConfig {

    @Value("${hipchat.access-token}")
    private String hipchatAccessToken;

    @Bean
    public HipChatClient hipChatClient(){
        return new HipChatClient(hipchatAccessToken);
    }
}
