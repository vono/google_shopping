package com.google.shopping.service.products;

import com.google.api.services.content.ShoppingContent;
import com.google.shopping.auth.Auth;
import com.google.shopping.domain.GMCCommon;
import com.google.shopping.domain.ResponseData;
import com.google.shopping.dto.products.AuthData;
import com.google.shopping.dto.products.ProductsDto;
import com.google.shopping.exception.GoogleProductException;
import com.google.shopping.utils.FileUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by user on 2017-11-29.
 */
@RequiredArgsConstructor
@Service
@Slf4j
public class ProductService {
    private final Auth auth;
    private final FileUtils fileUtils;
    private final AuthData authData;

    public ResponseData addProducts(String merchantId, String fileUrl) {
        ResponseData responseData = new ResponseData();

        ShoppingContent content = auth.getCredential(merchantId);
        authData.setShoppingContent(content);
        authData.setMerchantId(Long.parseLong(merchantId));

        try {
            String result = fileUtils.downloadFiles(merchantId, fileUrl);
            List<ProductsDto> productsList = fileUtils.parseTSV(result, merchantId);
            if(productsList.size() > 0) {
                responseData.setCode(GMCCommon.SUCCESS_CODE);
                responseData.setMessage(GMCCommon.SUCCESS);
                responseData.setData(productsList);
            }else{
                responseData.setCode(GMCCommon.FAIL_CODE);
                responseData.setMessage(GMCCommon.FAIL);
            }
        }catch (Exception e){
            throw new GoogleProductException(e.getMessage());
        }

        return responseData;
    }
}

