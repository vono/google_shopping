package com.google.shopping.service.products;

import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.google.api.services.content.model.Error;
import com.google.api.services.content.model.*;
import com.google.shopping.domain.GMCCommon;
import com.google.shopping.dto.products.AuthData;
import com.google.shopping.dto.products.ProductsDto;
import com.google.shopping.exception.GoogleProductException;
import com.google.shopping.utils.ReportUtils;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * Created by user on 2017-12-12.
 */
@RequiredArgsConstructor
@Service
@Slf4j
public class UploadService {
    private final ReportUtils reportUtils;
    private final AuthData authData;

    private static final String CHANNEL = "online";
    private static final String CONTENT_LANGUAGE = "en";
    private static final String TARGET_COUNTRY = "US";
    private static final String CURRENCY = "USD";

    public void uploadProducts(List<ProductsDto> productsList){
        Map<String, Object> listMap = new HashMap<>();
        List<ProductsCustomBatchRequestEntry> productsBatchRequestEntries = new ArrayList<>();
        ProductsCustomBatchRequest batchRequest = new ProductsCustomBatchRequest();
        ProductsCustomBatchResponse batchResponse;

        for (ProductsDto productEntity : productsList) {
            Product product = new Product();
            product.setOfferId(productEntity.getId());
            product.setTitle(productEntity.getTitle());
            product.setDescription(productEntity.getDescription());
//                product.setLink(productEntity.getLink());
            product.setLink("http://www.wisebirds.co/");
            product.setImageLink(productEntity.getImageLink());
            product.setContentLanguage(CONTENT_LANGUAGE);
            product.setTargetCountry(TARGET_COUNTRY);
            product.setChannel(CHANNEL);
            product.setAvailability(productEntity.getAvailability());
            product.setProductType(productEntity.getProductType());
            product.setBrand(productEntity.getBrand());
            product.setCondition(productEntity.getCondition());
            product.setAdult(productEntity.getAdult());


            //For Test
            product.setGtin("3234567890126");
            product.setMpn("GO12345OOGLE");

            Price price = new Price();
            price.setValue(productEntity.getPrice());
            price.setCurrency(CURRENCY);
            product.setPrice(price);

            ProductsCustomBatchRequestEntry entry = new ProductsCustomBatchRequestEntry();
            entry.setBatchId(productEntity.getBatchId());
            entry.setMerchantId(BigInteger.valueOf(authData.getMerchantId()));
            entry.setProduct(product);
            entry.setMethod("insert");
            productsBatchRequestEntries.add(entry);
            listMap.put(String.valueOf(productEntity.getBatchId()), productEntity);
        }
        batchRequest.setEntries(productsBatchRequestEntries);
        try {
            batchResponse = authData.getShoppingContent().products().custombatch(batchRequest).execute();
        }catch (IOException e){
            reportUtils.messageWithFail(GMCCommon.GOOGLE_PRODUCT_UPLOAD, e.getMessage());
            throw new GoogleProductException(e.getMessage(), e);
        }

        if(batchResponse != null){
            boolean hasError = false;
            for (ProductsCustomBatchResponseEntry entry : batchResponse.getEntries()) {
                if (entry.getProduct() != null) {
                    Product product = entry.getProduct();
                    System.out.println("success : " + product.getOfferId());
                    List<Error> warnings = product.getWarnings();
                    if (warnings != null) {
                        log.debug("There are %d warnings%n", warnings.size());
                    }
                } else if (entry.getErrors() != null) {
                    hasError = true;
                    String batchId = String.valueOf(entry.getBatchId());
                    ProductsDto productData = (ProductsDto)listMap.get(batchId);
                    reportUtils.uploadFail(entry.getErrors(), productData, false);
                }
            }
            if(hasError){
                reportUtils.uploadFail(new Errors(), new ProductsDto(), true);
            }
        }

    }

}
