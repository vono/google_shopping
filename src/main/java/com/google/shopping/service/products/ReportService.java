package com.google.shopping.service.products;

import com.google.shopping.domain.*;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 2017-12-20.
 */
@RequiredArgsConstructor
@Service
public class ReportService {
    private final HistoryRepository historyRepository;
    private final CommonErrorRepository commonErrorRepository;

    public ResponseData getFailedData(String merchantId, String searchDate, String reportType) {
        ResponseData responseData = new ResponseData();
        List<CommonError> commonErrors = new ArrayList<>();
        List<History> historyData = historyRepository.findByMerchantIdAndTypeAndTimeStartingWith(merchantId, reportType, searchDate);

        if (historyData != null && historyData.size() > 0) {
            String historyId = "";

            //TODO : sort 후 0번째꺼로 setting
            for (History history : historyData) {
                historyId = history.getId();
            }
            commonErrors = commonErrorRepository.findByHistoryId(historyId);
            responseData.setCode(GMCCommon.SUCCESS_CODE);
            responseData.setMessage(GMCCommon.SUCCESS);
            responseData.setData(commonErrors);
        }

        if (commonErrors.size() == 0) {
            responseData.setCode(GMCCommon.FAIL_CODE);
            responseData.setMessage("NO DATA !!!");
        }
        return responseData;
    }

    public ResponseData getHistoryData(String merchantId, String searchDate) {
        ResponseData responseData = new ResponseData();
        List<History> historyData = historyRepository.findByMerchantIdAndTimeStartingWith(merchantId, searchDate);

        if (historyData != null && historyData.size() > 0) {
            responseData.setCode(GMCCommon.SUCCESS_CODE);
            responseData.setMessage(GMCCommon.SUCCESS);
            responseData.setData(historyData);
        } else {
            responseData.setCode(GMCCommon.FAIL_CODE);
            responseData.setMessage("NO DATA !!!");
        }

        return responseData;
    }

}
