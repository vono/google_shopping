package com.google.shopping.service.member;

import com.amazonaws.services.s3.model.CompleteMultipartUploadRequest;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.shopping.domain.GMCCommon;
import com.google.shopping.domain.GmcMember;
import com.google.shopping.domain.GmcMemberRepository;
import com.google.shopping.domain.ResponseData;
import com.google.shopping.exception.GoogleProductException;
import com.google.shopping.utils.S3Utils;
import lombok.RequiredArgsConstructor;
import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by user on 2017-12-27.
 */
@RequiredArgsConstructor
@Service
public class MemberService {
    private final GmcMemberRepository gmcMemberRepository;
    private final S3Utils s3Utils;

    public ResponseData setMemberData(String merchantId, MultipartFile keyFile){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String currentTime = dateFormat.format(new Date(System.currentTimeMillis()));

        GmcMember gmcMember = new GmcMember();
        gmcMember.setTime(currentTime);
        gmcMember.setMerchantId(merchantId);
        gmcMember.setKeyfileName(keyFile.getOriginalFilename());
        try {
            InputStream inputStream = keyFile.getInputStream();
            JsonParser jsonParser = new JsonParser();
            JsonObject jsonObject = (JsonObject) jsonParser.parse(new InputStreamReader(inputStream, "UTF-8"));
            gmcMember.setKeyfile(jsonObject.toString());
        }catch (Exception e){
            throw new GoogleProductException(e.getMessage(), e);
        }

        GmcMember response = gmcMemberRepository.save(gmcMember);
        ResponseData responseData = new ResponseData();
        if(response != null && response.getId() != null){
            responseData.setCode(GMCCommon.SUCCESS_CODE);
            responseData.setMessage(GMCCommon.SUCCESS);
        }else{
            responseData.setCode(GMCCommon.FAIL_CODE);
            responseData.setMessage(GMCCommon.FAIL);
        }

        uploadKeyFile(merchantId, keyFile);

        return responseData;
    }

    private void uploadKeyFile(String merchantId, MultipartFile keyFile) {
        String folderName = merchantId + "/keyfile/";
        String fileName = folderName + keyFile.getOriginalFilename();

        try {
            InputStream inputStream = keyFile.getInputStream();
            byte[] bytes = IOUtils.toByteArray(inputStream);
            CompleteMultipartUploadRequest upload = s3Utils.upload(bytes, fileName);
            upload.getUploadId();

        }catch (IOException e){
            throw new GoogleProductException(e.getMessage(), e);
        }


    }

    public GmcMember getMemberData(String merchantId) {
        GmcMember gmcMember = gmcMemberRepository.findByMerchantId(merchantId);
        if(gmcMember != null && gmcMember.getKeyfileName() != null){
            return gmcMember;
        }else{
            throw new GoogleProductException("NO KEY FILE");
        }

    }
}
