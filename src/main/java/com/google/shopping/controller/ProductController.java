package com.google.shopping.controller;

import com.google.shopping.domain.ResponseData;
import com.google.shopping.dto.products.GmcDto;
import com.google.shopping.service.products.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * Created by user on 2017-11-30.
 */

@RestController
@RequiredArgsConstructor
@RequestMapping("/shopping")
public class ProductController {
    private final ProductService productService;

    @PostMapping("/products")
    public ResponseData addProducts(@RequestBody GmcDto.ProductRequest data){
        return productService.addProducts(data.getMerchantId(), data.getFileUrl());
    }
}
