package com.google.shopping.controller;

import com.google.shopping.domain.GMCCommon;
import com.google.shopping.domain.ResponseData;
import com.google.shopping.service.products.ReportService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by user on 2017-12-20.
 */

@RestController
@RequiredArgsConstructor
@RequestMapping("/report")
public class ReportController {
    private final ReportService reportService;

    @GetMapping("/parsing/{merchantId}/{date}")
    public ResponseData getParsingData(@PathVariable String merchantId, @PathVariable String date) {
        return reportService.getFailedData(merchantId, date, GMCCommon.EP_TSV_PARSING);
    }

    @GetMapping("/upload/{merchantId}/{date}")
    public ResponseData getUploadFailData(@PathVariable String merchantId, @PathVariable String date) {
        return reportService.getFailedData(merchantId, date, GMCCommon.GOOGLE_PRODUCT_UPLOAD);
    }


    @GetMapping("/history/{merchantId}/{date}")
    public ResponseData getHistoryData(@PathVariable String merchantId, @PathVariable String date) {
        return reportService.getHistoryData(merchantId, date);
    }

}
