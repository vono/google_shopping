package com.google.shopping.controller;

import com.google.shopping.domain.GMCCommon;
import com.google.shopping.domain.ResponseData;
import com.google.shopping.service.member.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;

/**
 * Created by user on 2017-12-27.
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/account")
public class MemberController {
    private final MemberService memberService;

    @PostMapping("/member")
    public ResponseData setMemberData(String merchantId, @Valid MultipartFile keyFile){
        if(keyFile != null){
            return memberService.setMemberData(merchantId, keyFile);
        }else{
            ResponseData responseData = new ResponseData();
            responseData.setCode(GMCCommon.FAIL_CODE);
            responseData.setMessage("NO FILE");
            return responseData;
        }
    }
}
