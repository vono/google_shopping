package com.google.shopping.domain;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAutoGeneratedKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import lombok.Data;

/**
 * Created by user on 2017-12-27.
 */

@Data
@DynamoDBTable(tableName = "gmc_member")
public class GmcMember {

    @DynamoDBHashKey
    @DynamoDBAutoGeneratedKey
    private String id;

    @DynamoDBAttribute(attributeName = "merchant_id")
    private String merchantId;

    @DynamoDBAttribute(attributeName = "keyfile_name")
    private String keyfileName;

    private String keyfile;

    private String time;

    @DynamoDBAttribute(attributeName = "application_name")
    private String applicationName;

    public String getApplicationName(){
        return this.getKeyfileName().substring(0, this.getKeyfileName().indexOf("."));
    }

}
