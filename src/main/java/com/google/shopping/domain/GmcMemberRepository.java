package com.google.shopping.domain;

import com.github.wonwoo.dynamodb.repository.DynamoDBRepository;
import org.socialsignin.spring.data.dynamodb.repository.EnableScan;

/**
 * Created by user on 2017-12-27.
 */
@EnableScan
public interface GmcMemberRepository extends DynamoDBRepository<GmcMember, String> {
    GmcMember findByMerchantId (String merchantId);
}
