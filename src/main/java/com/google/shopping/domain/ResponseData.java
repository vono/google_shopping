package com.google.shopping.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Created by user on 2017-12-21.
 */

@Getter
@Setter
public class ResponseData {
    private int code;
    private String message;
    private List<?> data;
}
