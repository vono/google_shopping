package com.google.shopping.domain;

import com.github.wonwoo.dynamodb.repository.DynamoDBRepository;
import org.socialsignin.spring.data.dynamodb.repository.EnableScan;

import java.util.List;

/**
 * Created by user on 2017-12-15.
 */
@EnableScan
public interface HistoryRepository extends DynamoDBRepository<History, String> {

    List<History> findByMerchantIdAndTimeStartingWith(String merchantId, String time);

    List<History> findByMerchantIdAndTypeAndTimeStartingWith(String merchantId, String type, String time);
}
