package com.google.shopping.domain;

/**
 * Created by user on 2017-12-20.
 */
public class GMCCommon {

    public static final int SUCCESS_CODE = 200;
    public static final int FAIL_CODE = 404;

    public static final String SUCCESS = "success";
    public static final String FAIL = "fail";

    public static final String GOOGLE_PRODUCT_UPLOAD = "ProductUpload";
    public static final String EP_TSV_PARSING = "TSVParsing";
    public static final String GOOGLE_AUTH = "Auth";
    public static final String EP_DOWNLOAD = "Download";

}
